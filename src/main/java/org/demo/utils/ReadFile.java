package org.demo.utils;

import org.demo.PresentarResultadosControlador;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Created by Roberto on 7/3/2017.
 */
public class ReadFile {
    private static ReadFile ourInstance = new ReadFile();

    public static ReadFile getInstance() {
        return ourInstance;
    }

    private ReadFile() {
    }

    /**
     * Lee un archivo y retorna un stream
     * @param nameFile
     * @return
     */
    public Stream<String> readLines(String nameFile){
        URL resource = PresentarResultadosControlador.class.getResource(nameFile);
        Path path = null;
        try {
            path = Paths.get(resource.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        try {
            return Files.lines(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
