package org.demo.utils;

/**
 * Created by Roberto on 9/3/2017.
 */
public enum MinutoCharla {

    MANIANA(180), TARDE(240), MARGENERROR(10);

    int valor;
    MinutoCharla(int valor) {
        this.valor = valor;
    }
    int minutos() {
        return valor;
    }
}
