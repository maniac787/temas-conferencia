package org.demo.utils;

import org.demo.ConferenciaBuilder;
import org.demo.entities.Charla;
import org.demo.entities.Conferencia;
import org.demo.entities.ResultadoPermutacion;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Roberto on 8/3/2017.
 */
public class MotorConferencia {
    private static MotorConferencia ourInstance = new MotorConferencia();

    public static MotorConferencia getInstance() {
        return ourInstance;
    }

    public static void main(String args[]) {
        ourInstance.generarConferencia("/lista-tematica.csv", 1);
    }

    private MotorConferencia() {
    }

    /**
     * Metodo general que permite generar una conferencia su temasticas sessiones y charlas
     * conferencia->tematicas-> sessiones-> charlas
     *
     * @param rutaArchivoConfiguracion
     * @param numeroTematicas
     * @return
     */
    public Conferencia generarConferencia(String rutaArchivoConfiguracion, int numeroTematicas) {

        Conferencia conferencia = construirInstancia();
        ResultadoPermutacion resultadoPermutacion = configurarSesion(rutaArchivoConfiguracion);

        conferencia.getTematicas().getSesionManiana().setCharlas(resultadoPermutacion.getCharlasManiana());
        conferencia.getTematicas().getSesionManiana().setCharlas(resultadoPermutacion.getCharlasTarde());
        return conferencia;
    }

    /**
     * @param rutaArchivoConfiguracion
     */
    private ResultadoPermutacion configurarSesion(String rutaArchivoConfiguracion) {
        List<Charla> charlas = cargarCharlas(rutaArchivoConfiguracion);

        /*
            180min de 9 a 12 AM y 240min de 1 a 5 PM
            En total 420min = 7h
        */
        int minutosMaximos = MinutoCharla.MANIANA.minutos() + MinutoCharla.TARDE.minutos();

        //Tipos para escoger
        int n = calcularIndiceCharlasTiempo(charlas, minutosMaximos);

        //Solo toma los elementos que alcanzan en el tiempo 420mins
        removerElemntosPorTiempo(charlas, n);

        int r = charlas.size();   //Elementos elegidos, para el numero de permutaciones
        int minutos = 0;
        ResultadoPermutacion resultadoPermutacion = new ResultadoPermutacion();
        permutarCharlas(charlas, "", n, r, minutos,
                MinutoCharla.MANIANA.minutos(), MinutoCharla.MARGENERROR.minutos(), resultadoPermutacion);

        resultadoPermutacion = imprimirCharlas(resultadoPermutacion.getCadenaPermutada(), charlas,
                MinutoCharla.MANIANA.minutos(), MinutoCharla.TARDE.minutos(), MinutoCharla.MARGENERROR.minutos());

        return resultadoPermutacion;
    }

    /**
     * @param rutaArchivoConfiguracion
     * @return
     */
    private List<Charla> cargarCharlas(String rutaArchivoConfiguracion) {
        Stream<String> stream = ReadFile.getInstance().readLines(rutaArchivoConfiguracion);
        List<Charla> charlas = ParseObject.getInstance().generateList(stream);

        for (int i = 0; i < charlas.size(); i++) {
            charlas.get(i).setCodigo(String.valueOf(i + 1));
        }
        return charlas;
    }

    /**
     * Permite constriur una instancia con todos los objetos necesarios
     *
     * @return
     */
    private Conferencia construirInstancia() {
        return ConferenciaBuilder.getInstance().crearConferencia();
    }


    /**
     * Permite la impresion de las charlas  ala vez que retorna un objeto de rultado del proceso
     *
     * @param cadenaResultadoPermutacion
     * @param charlas
     * @param minutosMaximosManiana
     * @param minutosMaximosTarde
     * @param margenError
     * @return
     */
    private ResultadoPermutacion imprimirCharlas(String cadenaResultadoPermutacion, List<Charla> charlas,
                                                 int minutosMaximosManiana, int minutosMaximosTarde,
                                                 int margenError) {
        ResultadoPermutacion resultadoPermutacion = new ResultadoPermutacion();
        int sumaPermutacionManiana = 0;
        int sumaPermutacionTarde = 0;
        int indiceListas = 0;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 9);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        String[] permmutacionCharla = cadenaResultadoPermutacion.split(", ");
        Collections.reverse(Arrays.asList(permmutacionCharla));

        System.out.println("\n**********************************************************************");
        System.out.println("* Tematica 1 *");
        System.out.println("**********************************************************************");


        for (; indiceListas < permmutacionCharla.length; indiceListas++) {
            System.out.println(
                    minutosHoras(calendar, charlas.get(Integer.valueOf(permmutacionCharla[indiceListas].trim()) - 1).getMinutos(), "AM", indiceListas) +
                            charlas.get(Integer.valueOf(permmutacionCharla[indiceListas].trim()) - 1).getNombre() + " " +
                            charlas.get(Integer.valueOf(permmutacionCharla[indiceListas].trim()) - 1).getMinutos() + " min");

            sumaPermutacionManiana += charlas.get(Integer.valueOf(permmutacionCharla[indiceListas].trim()) - 1).getMinutos();

            //Agregamos el resultado al el objeto de resultados
            resultadoPermutacion.getCharlasManiana().add(charlas.get(Integer.valueOf(permmutacionCharla[indiceListas].trim()) - 1));
            if ((sumaPermutacionManiana >= (minutosMaximosManiana - margenError)) &&
                    (sumaPermutacionManiana <= (minutosMaximosManiana + margenError))) {
                break;
            }
        }

        System.out.println("Para el primer caso se usaron " + sumaPermutacionManiana + " minutos de " +
                minutosMaximosManiana + " tras ordenar lo mejor posible los valores");


        System.out.println("\n********************************************************************");
        System.out.println("* Tematica 2 *");
        System.out.println("********************************************************************");
        int indiceSession2 = 0;
        calendar.set(Calendar.HOUR, 13);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        for (int j = indiceListas + 1; j < permmutacionCharla.length; j++) {
            sumaPermutacionTarde += charlas.get(Integer.valueOf(permmutacionCharla[j].trim()) - 1).getMinutos();

            //Agregamos el resultado al el objeto de resultados
            resultadoPermutacion.getCharlasTarde().add(charlas.get(Integer.valueOf(permmutacionCharla[j].trim()) - 1));

            System.out.println(
                    minutosHoras(calendar, charlas.get(Integer.valueOf(permmutacionCharla[indiceListas].trim()) - 1).getMinutos(), "AM", indiceSession2) +
                            charlas.get(Integer.valueOf(permmutacionCharla[j].trim()) - 1).getNombre() + " " +
                            charlas.get(Integer.valueOf(permmutacionCharla[j].trim()) - 1).getMinutos() + " min");
            indiceSession2++;
        }

        System.out.println("Para el primer caso se usaron " + sumaPermutacionTarde + " minutos de " +
                minutosMaximosTarde + " tras ordenar lo mejor posible los valores");

        return resultadoPermutacion;

    }

    /**
     * Incrementa los minutos
     *
     * @param calendar
     * @param minutes
     * @param subFijo
     * @return
     */
    private String minutosHoras(Calendar calendar, int minutes, String subFijo, int indice) {
        calendar.set(Calendar.SECOND, 0);
        if (indice == 0) {
            calendar.add(Calendar.MINUTE, minutes * -1);
        }
        calendar.add(Calendar.MINUTE, minutes);
        return calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + " " + subFijo + " ";
    }

    /**
     * Elimina de la lista de charlas aquellos elementos que se pasan del tiempo
     *
     * @param charlas
     * @param n
     */
    private void removerElemntosPorTiempo(List<Charla> charlas, int n) {
        int tamanioListaCharla = charlas.size() - 1;
        for (int i = tamanioListaCharla; i > 0; i--) {
            if (i >= n) {
                charlas.remove(i);
            } else {
                break;
            }
        }
    }

    /**
     * Metodo principal que se encarga de forma recursiva calcular la mejor opcion en cuestion de tiempos
     * REaliza la permutacion pero a su vez tambien verifica que la permutacion sea la mas adecuada, se toma como funcion
     * el tiempo en minutos de las charlas de la manian y las de la tarde, agregando un margen de error de 10 minutos
     *
     * @param charlas
     * @param cadenaPermutacionCharla
     * @param n
     * @param r
     * @param minutos
     * @param minutosParada
     * @param margenError
     * @param resultadoPermutacion
     */
    private void permutarCharlas(List<Charla> charlas, String cadenaPermutacionCharla, int n, int r, int minutos,
                                 int minutosParada, int margenError, ResultadoPermutacion resultadoPermutacion) {

        if (n == 0) {
            //System.out.println(act + "  - Minutos " + minutos);
            resultadoPermutacion.setCadenaPermutada(cadenaPermutacionCharla);
        } else {
            for (int i = 0; i < r; i++) {
                if (!cadenaPermutacionCharla.contains(charlas.get(i).getCodigo())) { // Controla que no haya repeticiones
                    permutarCharlas(charlas, cadenaPermutacionCharla + charlas.get(i).getCodigo() + ", ",
                            n - 1, r, minutos + charlas.get(i).getMinutos(), minutosParada, margenError, resultadoPermutacion);

                    if ((minutos >= (minutosParada - margenError)) && (minutos <= (minutosParada + margenError))) {
                        //System.out.println(act + "  - Minutos " + minutos);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Permite tomar una muestra de la lista de charlas que se rige en base al tiempo
     *
     * @param charlas
     * @param minutosMaximos
     * @return
     */
    private int calcularIndiceCharlasTiempo(List<Charla> charlas, int minutosMaximos) {
        int indiceTopeCharlaDia = 0;
        int numeroMinutos = 0;
        for (Charla charla : charlas) {
            if (numeroMinutos <= minutosMaximos) {
                if (numeroMinutos + charla.getMinutos() < minutosMaximos) {
                    numeroMinutos += charla.getMinutos();
                    indiceTopeCharlaDia++;
                } else {
                    break;
                }
            } else {
                break;
            }
        }

        System.out.println("Se toman " + numeroMinutos + " minutos de los 420 minutos habiles");
        System.out.println("Se tomaran " + indiceTopeCharlaDia + " de los " + charlas.size() +
                " elementos en la lista de charlas");
        return indiceTopeCharlaDia;
    }
}
