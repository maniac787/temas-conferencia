package org.demo.utils;

import org.demo.entities.Charla;
import org.demo.entities.Tematica;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Roberto on 7/3/2017.
 */
public class ParseObject {
    private static ParseObject ourInstance = new ParseObject();

    public static ParseObject getInstance() {
        return ourInstance;
    }

    private ParseObject() {
    }

    /**
     *
     * @param stream
     * @return
     */
    public LinkedList<Charla> generateList(Stream<String> stream) {
        LinkedList<Charla> tematicaList = new LinkedList<Charla>();
        stream.forEach(s -> {
            try {
                String[] lineaSeparada = s.split(",");
                tematicaList.add(new Charla(lineaSeparada[0], Integer.valueOf(lineaSeparada[1].trim())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return tematicaList;
    }
}
