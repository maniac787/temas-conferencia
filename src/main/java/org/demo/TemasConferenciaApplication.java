package org.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TemasConferenciaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TemasConferenciaApplication.class, args);
	}
}
