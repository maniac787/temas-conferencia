package org.demo;

import org.demo.entities.Conferencia;
import org.demo.entities.Sesion;
import org.demo.entities.Tematica;

import java.util.ArrayList;

/**
 * Created by Roberto on 7/3/2017.
 */
public class ConferenciaBuilder {
    private static ConferenciaBuilder ourInstance = new ConferenciaBuilder();

    public static ConferenciaBuilder getInstance() {
        return ourInstance;
    }

    private ConferenciaBuilder() {
    }

    public Conferencia crearConferencia() {
        Conferencia conferencia = new Conferencia();
        conferencia.setTematicas(new Tematica());

        conferencia.getTematicas().setSesionManiana(new Sesion());
        conferencia.getTematicas().getSesionManiana().setCharlas(new ArrayList<>());
        conferencia.getTematicas().setSesionTarde(new Sesion());
        conferencia.getTematicas().getSesionTarde().setCharlas(new ArrayList<>());


        return conferencia;
    }
}
