package org.demo;

import org.demo.entities.Conferencia;
import org.demo.utils.MotorConferencia;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by Roberto on 7/3/2017.
 */
@RestController
public class PresentarResultadosControlador {
    @RequestMapping(value = "/conferencia", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    /**
     * Servicio rest que presenta la informacion generada
     */
    public Conferencia cargarDatos() throws IOException, URISyntaxException {
        Conferencia conferencia = null;
        try {
            conferencia = MotorConferencia.getInstance().generarConferencia("/lista-tematica.csv", 1);
            conferencia.setEstado("Conferencia creada");
        }catch (Exception ex){
            conferencia = new Conferencia();
            conferencia.setEstado("Ocurrio un error al configurar la conferencia");
        }

        return conferencia;
    }
}