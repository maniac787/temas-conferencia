package org.demo.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roberto on 9/3/2017.
 */
public class ResultadoPermutacion {
    private String cadenaPermutada;

    private List<Charla> charlasManiana;
    private List<Charla> charlasTarde;

    public ResultadoPermutacion() {
        charlasManiana = new ArrayList<Charla>();
        charlasTarde = new ArrayList<Charla>();
    }

    public String getCadenaPermutada() {
        return cadenaPermutada;
    }

    public void setCadenaPermutada(String cadenaPermutada) {
        this.cadenaPermutada = cadenaPermutada;
    }

    public List<Charla> getCharlasManiana() {
        return charlasManiana;
    }

    public void setCharlasManiana(List<Charla> charlasManiana) {
        this.charlasManiana = charlasManiana;
    }

    public List<Charla> getCharlasTarde() {
        return charlasTarde;
    }

    public void setCharlasTarde(List<Charla> charlasTarde) {
        this.charlasTarde = charlasTarde;
    }
}
