package org.demo.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by Roberto on 8/3/2017.
 */
public class Charla {
    @JsonIgnore
    private String codigo;
    private String nombre;
    private Integer minutos;

    public Charla() {
    }

    public Charla(String codigo, String nombre, Integer minutos) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.minutos = minutos;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Charla(String nombre, Integer minutos) {
        this.nombre = nombre;
        this.minutos = minutos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getMinutos() {
        return minutos;
    }

    public void setMinutos(Integer minutos) {
        this.minutos = minutos;
    }

    @Override
    public String toString() {
        return " "+ minutos + " ";
    }
}
