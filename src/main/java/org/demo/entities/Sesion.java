package org.demo.entities;

import java.util.List;

/**
 * Created by Roberto on 8/3/2017.
 */
public class Sesion {

    private List<Charla> charlas;

    public Sesion() {
    }

    public List<Charla> getCharlas() {
        return charlas;
    }

    public void setCharlas(List<Charla> charlas) {
        this.charlas = charlas;
    }
}

