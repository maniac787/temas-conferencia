package org.demo.entities;

import javax.websocket.Session;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Roberto on 7/3/2017.
 */
public class Tematica implements Serializable{

    private Sesion sesionManiana;
    private Sesion sesionTarde;


    public Tematica() {
    }

    public Tematica(Sesion sesionManiana, Sesion sesionTarde) {
        this.sesionManiana = sesionManiana;
        this.sesionTarde = sesionTarde;
    }

    public Sesion getSesionManiana() {
        return sesionManiana;
    }

    public void setSesionManiana(Sesion sesionManiana) {
        this.sesionManiana = sesionManiana;
    }

    public Sesion getSesionTarde() {
        return sesionTarde;
    }

    public void setSesionTarde(Sesion sesionTarde) {
        this.sesionTarde = sesionTarde;
    }
}
