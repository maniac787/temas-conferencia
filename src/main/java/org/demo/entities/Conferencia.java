package org.demo.entities;

import java.util.List;

/**
 * Created by Roberto on 8/3/2017.
 */
public class Conferencia {
    private String estado;

    private Tematica tematicas;

    public Tematica getTematicas() {
        return tematicas;
    }

    public void setTematicas(Tematica tematicas) {
        this.tematicas = tematicas;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
