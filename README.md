##Roberto Chasipanta / maniac787@gmail.com / 0995361740

#Instrucciones
- Usar Java 8
- Instalar maven
- ejecutar el commando
- - mvn spring-boot:run
- probar en el navegador con la siguiente direccion
- http://localhost:8080/conferencia

- Para un test en consola ejecutar la clase TemasConferenciaApplicationTests.java
- Para agregar o quitar elementos de la lista en la ruta de resources en el archivo csv


##Resoluci'on
La idea de la resolución del problema es calcular la forma mas optima en la que puede quedar ordenada la lista de charlas,
tambie se toma en cuneta que la lista que se tiene puede sobrepasar las 7 horas que se disponen al dia, por lo cual se toma una muestra que estre en el rango.

 - En funcion de esta premisa se uso permutaciones para poder calcular un numero finito de combinaciones,
    sobre las cuales se aplica una validacion de tiempo, es decir pueden haber mas de una permutaciones que se acerquen a lo optimo
     para llegar a este punto se agrego un margen de error de 10 minutos los mismo que se encuentran configuradois en una enumeracion.
     Cuando la permutacion se acerca a lo optimo se retorna un objeto de resulta en el que se almacena la permutacion para su respectiva impresion.

Consideraciones.
    Si intentamos sacar todas las permutaciones posibles en la lista completa la pc tiene una sobrecarga de trabajo,
    por este tema se valida por tiempo si tenemos 100 items y son  30 horas solo debemos tomar el numero que corresponda a las 7 que tiene el dia,
    y desechar para el ejemplo el resto de items, en otros casos se puede ir apilando los cursos y usarlos para los respectivos dias consecuentes,
    ademas cuando se encuentra una combinacion candidata se rompre el lazo de permutacion.
